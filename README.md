Desafio:

Crie uma aplicação em nodeJS ou php com testes de unidade para utilizar alguma função da API Totalvoice (https://api.totalvoice.com.br/doc/). Não é permitido usar nenhuma biblioteca ou método de auxílio para as requisições HTTP (quais sejam curl, fopen, syscall, etc). Você deve utilizar apenas socket e escrever e tratar todas as requisições HTTP. O objetivo do teste é medir seus conhecimentos no protocolo e habilidades de desenvolvimento na linguagem. O código deve estar documentado e possuir instruções de uso da aplicação.

Aplicação:

Foi desenvolvida uma aplicação em php para fazer o envio de SMS através da API da TotalVoice(https://api.totalvoice.com.br/doc/).
Os testes unitários utilizam o PHPUnit.